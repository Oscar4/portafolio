import { Component } from '@angular/core';
import { GithubServices } from '../../app/services/github';
import { NavController } from 'ionic-angular';
import { DetailsPage } from '../details/details.ts';

@Component({
  templateUrl: 'home.html'
})
export class HomePage {
  public foundRepos;
  public username;

  constructor(private github: GithubServices, 
    private nav: NavController) {
  }

  getRepos(){
    this.github.getRepos(this.username).subscribe(
      data => {
        this.foundRepos = data.json();
      },
      err => console.log("Hubo un error"),
      () => console.log("Repositorios conseguidos")
    );
  }

  goToDetails(repo){
    
    this.nav.push(DetailsPage, {repo: repo});
  }

}