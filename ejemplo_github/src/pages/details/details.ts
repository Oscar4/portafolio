import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


@Component({
  templateUrl: 'build/pages/details/details.html'
})
export class DetailsPage {
  public readme = "";
  public repo;

  constructor(
    
    public navCtrl: NavController, 
    public navParams: NavParams) {

      this.repo = navParams.get('repo');
      /*this.github.getDetails(this.repo).subscribe(
        data => this.readme = data.text(),
        err => {
          if (err.status == 404) {
            this.readme = "Este repo no tiene readme";
          } else {
            console.log("error");
          }
        }
      );*/
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailsPage');
  }

}
